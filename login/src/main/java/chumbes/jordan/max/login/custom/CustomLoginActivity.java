package chumbes.jordan.max.login.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by MB-01 on 26/07/2017.
 */

public class CustomLoginActivity extends RelativeLayout {
    public CustomLoginActivity(Context context) {
        super(context);
    }

    public CustomLoginActivity(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomLoginActivity(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void init(Context context)
    {

    }
}
