package chumbes.jordan.max.login.view;

/**
 * Created by MB-01 on 26/07/2017.
 */

public interface LoginContract {
    interface View{
        void successComplete();
    }
    interface Presenter {
        void authentication(String user,String password);
    }
}
