package chumbes.jordan.max.login.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import chumbes.jordan.max.login.R;

/**
 * Created by MB-01 on 26/07/2017.
 */

public class ActivityLogin extends AppCompatActivity implements View.OnClickListener,LoginContract.View{
    private EditText mUserEditText,mPasswordEditText;
    private Button mSendSinIn,mSendFaceBook;
    private LoginPresenter mLoginPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mUserEditText= (EditText) findViewById(R.id.user_edittext_input);
        mPasswordEditText = (EditText)findViewById(R.id.password_edittext_input);
        mSendSinIn = (Button) findViewById(R.id.sendSignIn);
        mSendFaceBook = (Button) findViewById(R.id.sendFaceBook);
        mLoginPresenter = new LoginPresenter(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sendSignIn:
                mLoginPresenter.authentication(mUserEditText.getText().toString(),
                        mPasswordEditText.getText().toString());
                break;
            case R.id.sendFaceBook:
                break;
            default:
                break;
        }
    }

    @Override
    public void successComplete() {

    }
}
